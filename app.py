from flask import Flask, request
import pymongo

# #### INIT SERVER #### #
app = Flask('app')

def build_mongo_setup(**input):

    mongo_host = "192.168.1.60"
    mongo_port = "27019"
    mongo_name = 'database_test'

    mongoclient = pymongo.MongoClient(host=mongo_host, port=int(mongo_port))
    mongo_db = getattr(mongoclient, mongo_name)

    if input.items():
        for key, value in input.items():

            if key == "server":

                # Add server.config
                value.config['MONGO_DBNAME'] = mongo_name
                value.config['MONGO_URI'] = f'mongodb://{mongo_host}:{mongo_port}/'

                return mongo_db, value

            if key == "return_client":
                if value == True:
                    return mongo_db, mongoclient
        
    return mongo_db

mongo_db, app = build_mongo_setup(server = app)

# API for Configurator
@app.route('/configrequest', methods=['GET'])
def create_configrequest():

    print("configrequest")

    data = request.json

    if data:

        print(f"RECEIVED POST:\n {data}")

    config = mongo_db.salesConfigs
    language = request.json['language']
    adress = request.json['adress']
    contact = request.json['contact']
    payment = request.json['payment']
    articles = request.json['articles']

    config_id = config.insert({
        'language': language, 
        'adress': adress, 
        'contact': contact, 
        'payment': payment, 
        'articles': articles, 
    })

    new_entry = config.find_one({'_id': config_id })
    # output = {
    #     'language': new_entry['language'], 
    #     'adress': new_entry['adress'], 
    #     'contact': new_entry['contact'], 
    #     'payment': new_entry['payment'], 
    #     'articles': new_entry['articles'], 
    # }

    return "OK"


if __name__ == "__main__":

    app.run(host='0.0.0.0', port=8302)
